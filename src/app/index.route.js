(function() {
  'use strict';

  angular
    .module('mclaren')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider, $locationProvider) {
    $stateProvider
      .state('home', {
        url: '/',
        templateUrl: 'app/main/main.html',
        controller: 'MainController',
        controllerAs: 'main',
        /*
          Used the resolve for providing all the information to the controller in order to render it.
         */
        resolve: {
          definitionActivities: function(backEnd){            
            return backEnd.get('definitions/activities');
          },
          patients: function(backEnd){            
            return backEnd.get('patients');
          },
          summary: function(backEnd){            
            return backEnd.get('patients/1/summary');
          }
        }
      });

      $urlRouterProvider.otherwise('/');

    $locationProvider.html5Mode({
      enabled: true,
      requireBase: false
    });
  }

})();
