(function() {
  'use strict';

  angular
    .module('mclaren')
    .directive('navbar', navbar);

  /*
    Directive needed for encapsulate a separate navbar e reuse it for avery state.
   */

  /** @ngInject */
  function navbar() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/components/navbar/navbar.html',
      scope: {
          title: '='
      },
      controller: NavbarController,
      controllerAs: 'vm',
      bindToController: true
    };

    return directive;

    /** @ngInject */
    function NavbarController() {
      
    }
  }

})();
