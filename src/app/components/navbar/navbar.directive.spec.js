(function() {
  'use strict';

  describe('directive navbar', function() {
    var vm;
    var el;
    var title;

    beforeEach(module('mclaren'));
    beforeEach(inject(function($compile, $rootScope) {
      title = 'title';

      el = angular.element('<navbar title="' + title + '"></navbar>');

      $compile(el)($rootScope.$new());
      $rootScope.$digest();
      vm = el.isolateScope().vm;
    }));

    it('should be compiled', function() {
      expect(el.html()).not.toEqual(null);
    });

    it('should have isolate scope object with instanciate members', function() {
      expect(vm).toEqual(jasmine.any(Object));
    });
  });
})();
