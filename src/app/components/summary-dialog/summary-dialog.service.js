(function() {
  'use strict';

  angular
    .module('mclaren')
    .factory('summaryDialog', summaryDialog);

  /** @ngInject */
  function summaryDialog($document, $mdDialog) {

    var service = {
      show: show
    };

    return service;

    /**
     * This function will show the dialog with the all the information provided.
     */
    function show(first_name, last_name, summary_completed) {
        var parentEl = angular.element($document.body);
        $mdDialog.show({
           parent: parentEl,
           templateUrl: 'app/components/summary-dialog/summary-dialog.tmpl.html',
           locals: {
             first_name: first_name,
             last_name: last_name,
             summary:  summary_completed
           },
           controller: DialogController
        });
        function DialogController($scope, $mdDialog, first_name, last_name, summary) {
          $scope.summary = summary;
          $scope.first_name = first_name;
          $scope.last_name = last_name;
          $scope.showDetail = false;

          $scope.closeDialog = function() {
            $mdDialog.hide();
          }

          $scope.showActivityDetail = function(activity_detail){
            $scope.showDetail= true;
            $scope.detail = activity_detail;
          }
        }      
    }
  }
})();
