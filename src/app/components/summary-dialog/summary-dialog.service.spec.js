(function() {
  'use strict';

  describe('service summaryDialog', function() {
    var summaryDialog;

    beforeEach(module('mclaren'));
    beforeEach(inject(function(_summaryDialog_) {
      summaryDialog = _summaryDialog_;
    }));

    it('should be registered', function() {
      expect(summaryDialog).not.toEqual(null);
    });

    describe('show function', function() {
      it('should exist', function() {
        expect(summaryDialog.show).not.toEqual(null);
      });

    });

  });
})();
