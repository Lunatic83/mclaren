(function() {
  'use strict';

  /*
    This interceptor is neede to catch all the request from the BackEnd service, in order to emulate the response from the BackEnd.
   */

  angular
    .module('mclaren')
    .factory('httpInterceptor', httpInterceptor);
  
  /** @ngInject */
  function httpInterceptor() {
    var patients_list = [
      {
        id:1,
        patient_id: "0000-4444",
        first_name: "John",
        last_name: "Doe",
        gender: "Male",
        date_of_birth: "1960-02-22",
        race: "White",
        language: "English",
        maritial_status: "Divorced",
        summary:[
          {
            date: "2016-12-08",
            activity_id: 2,
            result: "Good"
          },
          {
            date: "2016-12-07",
            activity_id: 2,
            result: "Good"
          },
          {
            date: "2016-12-06",
            activity_id: 1,
            result: "Good"
          },
          {
            date: "2016-12-05",
            activity_id: 2,
            result: "Good"
          },
          {
            date: "2016-12-04",
            activity_id: 2,
            result: "Good"
          },
          {
            date: "2016-12-03",
            activity_id: 3,
            result: "Not Good"
          },
          {
            date: "2016-12-02",
            activity_id: 3,
            result: "Good"
          },
          {
            date: "2016-12-01",
            activity_id: 1,
            result: "Good"
          },
          {
            date: "2016-11-31",
            activity_id: 2,
            result: "Not Good"
          },
          {
            date: "2016-11-30",
            activity_id: 2,
            result: "Not Good"
          },
          {
            date: "2016-11-29",
            activity_id: 1,
            result: "Good"
          },
          {
            date: "2016-11-28",
            activity_id: 2,
            result: "Good"
          },
          {
            date: "2016-11-27",
            activity_id: 2,
            result: "Not Good"
          }
        ]
      },
      {
        id:2,
        patient_id: "0000-4445",
        first_name: "Maria",
        last_name: "Ross",
        gender: "Female",
        date_of_birth: "1965-06-12",
        race: "Black",
        language: "English",
        maritial_status: "Single",
        summary:[
          {
            date: "2016-12-08",
            activity_id: 2,
            result: "Good"
          },
          {
            date: "2016-12-07",
            activity_id: 1,
            result: "Not Good"
          },
          {
            date: "2016-12-06",
            activity_id: 2,
            result: "Not Good"
          },
          {
            date: "2016-12-05",
            activity_id: 2,
            result: "Good"
          },
          {
            date: "2016-12-04",
            activity_id: 2,
            result: "Not Good"
          },
          {
            date: "2016-12-03",
            activity_id: 1,
            result: "Not Good"
          },
          {
            date: "2016-12-02",
            activity_id: 2,
            result: "Good"
          },
          {
            date: "2016-12-01",
            activity_id: 2,
            result: "Good"
          },
          {
            date: "2016-11-31",
            activity_id: 3,
            result: "Not Good"
          },
          {
            date: "2016-11-30",
            activity_id: 2,
            result: "Not Good"
          },
          {
            date: "2016-11-29",
            activity_id: 3,
            result: "Not Good"
          },
          {
            date: "2016-11-28",
            activity_id: 1,
            result: "Not Good"
          },
          {
            date: "2016-11-27",
            activity_id: 3,
            result: "Not Good"
          }
        ]
      },
      {
        id:3,
        patient_id: "0000-4446",
        first_name: "Christine",
        last_name: "Stone",
        gender: "Female",
        date_of_birth: "1945-06-12",
        race: "White",
        language: "English",
        maritial_status: "Single",
        summary:[
          {
            date: "2016-12-08",
            activity_id: 2,
            result: "Good"
          },
          {
            date: "2016-12-07",
            activity_id: 1,
            result: "Good"
          },
          {
            date: "2016-12-06",
            activity_id: 1,
            result: "Good"
          },
          {
            date: "2016-12-05",
            activity_id: 1,
            result: "Good"
          },
          {
            date: "2016-12-04",
            activity_id: 1,
            result: "Not Good"
          },
          {
            date: "2016-12-03",
            activity_id: 1,
            result: "Not Good"
          },
          {
            date: "2016-12-02",
            activity_id: 1,
            result: "Good"
          },
          {
            date: "2016-12-01",
            activity_id: 2,
            result: "Good"
          },
          {
            date: "2016-11-31",
            activity_id: 3,
            result: "Not Good"
          },
          {
            date: "2016-11-30",
            activity_id: 2,
            result: "Not Good"
          },
          {
            date: "2016-11-29",
            activity_id: 3,
            result: "Not Good"
          },
          {
            date: "2016-11-28",
            activity_id: 1,
            result: "Not Good"
          },
          {
            date: "2016-11-27",
            activity_id: 3,
            result: "Not Good"
          }
        ]
      },
      {
        id:4,
        patient_id: "0000-4447",
        first_name: "Mario",
        last_name: "Rossi",
        gender: "Male",
        date_of_birth: "1953-03-22",
        race: "White",
        language: "Italian",
        maritial_status: "Married",
        summary:[
          {
            date: "2016-12-08",
            activity_id: 1,
            result: "Not Good"
          },
          {
            date: "2016-12-07",
            activity_id: 2,
            result: "Good"
          },
          {
            date: "2016-12-06",
            activity_id: 3,
            result: "Good"
          },
          {
            date: "2016-12-05",
            activity_id: 4,
            result: "Good"
          },
          {
            date: "2016-12-04",
            activity_id: 1,
            result: "Not Good"
          },
          {
            date: "2016-12-03",
            activity_id: 2,
            result: "Not Good"
          },
          {
            date: "2016-12-02",
            activity_id: 3,
            result: "Good"
          },
          {
            date: "2016-12-01",
            activity_id: 2,
            result: "Good"
          },
          {
            date: "2016-11-31",
            activity_id: 3,
            result: "Not Good"
          },
          {
            date: "2016-11-30",
            activity_id: 2,
            result: "Not Good"
          },
          {
            date: "2016-11-29",
            activity_id: 3,
            result: "Not Good"
          },
          {
            date: "2016-11-28",
            activity_id: 1,
            result: "Not Good"
          },
          {
            date: "2016-11-27",
            activity_id: 3,
            result: "Not Good"
          }
        ]
      },
      {
        id:5,
        patient_id: "0000-4448",
        first_name: "Mark",
        last_name: "Anderson",
        gender: "Male",
        date_of_birth: "1957-03-22",
        race: "White",
        language: "English",
        maritial_status: "Married",
        summary:[
          {
            date: "2016-12-08",
            activity_id: 1,
            result: "Good"
          },
          {
            date: "2016-12-07",
            activity_id: 2,
            result: "Good"
          },
          {
            date: "2016-12-06",
            activity_id: 3,
            result: "Good"
          },
          {
            date: "2016-12-05",
            activity_id: 4,
            result: "Good"
          },
          {
            date: "2016-12-04",
            activity_id: 1,
            result: "Not Good"
          },
          {
            date: "2016-12-03",
            activity_id: 2,
            result: "Not Good"
          },
          {
            date: "2016-12-02",
            activity_id: 3,
            result: "Good"
          },
          {
            date: "2016-12-01",
            activity_id: 2,
            result: "Good"
          },
          {
            date: "2016-11-31",
            activity_id: 3,
            result: "Not Good"
          },
          {
            date: "2016-11-30",
            activity_id: 2,
            result: "Not Good"
          },
          {
            date: "2016-11-29",
            activity_id: 3,
            result: "Not Good"
          },
          {
            date: "2016-11-28",
            activity_id: 1,
            result: "Not Good"
          },
          {
            date: "2016-11-27",
            activity_id: 3,
            result: "Not Good"
          }
        ]
      },
      {
        id:6,
        patient_id: "0000-4449",
        first_name: "Stewart",
        last_name: "Doer",
        gender: "Male",
        date_of_birth: "1967-09-12",
        race: "White",
        language: "English",
        maritial_status: "Married",
        summary:[
          {
            date: "2016-12-08",
            activity_id: 1,
            result: "Not Good"
          },
          {
            date: "2016-12-07",
            activity_id: 2,
            result: "Good"
          },
          {
            date: "2016-12-06",
            activity_id: 3,
            result: "Not Good"
          },
          {
            date: "2016-12-05",
            activity_id: 4,
            result: "Good"
          },
          {
            date: "2016-12-04",
            activity_id: 1,
            result: "Not Good"
          },
          {
            date: "2016-12-03",
            activity_id: 2,
            result: "Not Good"
          },
          {
            date: "2016-12-02",
            activity_id: 3,
            result: "Good"
          },
          {
            date: "2016-12-01",
            activity_id: 2,
            result: "Good"
          },
          {
            date: "2016-11-31",
            activity_id: 3,
            result: "Not Good"
          },
          {
            date: "2016-11-30",
            activity_id: 2,
            result: "Not Good"
          },
          {
            date: "2016-11-29",
            activity_id: 3,
            result: "Not Good"
          },
          {
            date: "2016-11-28",
            activity_id: 1,
            result: "Not Good"
          },
          {
            date: "2016-11-27",
            activity_id: 3,
            result: "Not Good"
          }
        ]
      },
      {
        id:7,
        patient_id: "0000-4450",
        first_name: "Mery",
        last_name: "White",
        gender: "Female",
        date_of_birth: "1951-12-12",
        race: "White",
        language: "English",
        maritial_status: "Married",
        summary:[
          {
            date: "2016-12-08",
            activity_id: 1,
            result: "Not Good"
          },
          {
            date: "2016-12-07",
            activity_id: 2,
            result: "Good"
          },
          {
            date: "2016-12-06",
            activity_id: 3,
            result: "Not Good"
          },
          {
            date: "2016-12-05",
            activity_id: 4,
            result: "Good"
          },
          {
            date: "2016-12-04",
            activity_id: 1,
            result: "Not Good"
          },
          {
            date: "2016-12-03",
            activity_id: 2,
            result: "Not Good"
          },
          {
            date: "2016-12-02",
            activity_id: 3,
            result: "Good"
          },
          {
            date: "2016-12-01",
            activity_id: 2,
            result: "Good"
          },
          {
            date: "2016-11-31",
            activity_id: 3,
            result: "Not Good"
          },
          {
            date: "2016-11-30",
            activity_id: 2,
            result: "Not Good"
          },
          {
            date: "2016-11-29",
            activity_id: 3,
            result: "Not Good"
          },
          {
            date: "2016-11-28",
            activity_id: 1,
            result: "Not Good"
          },
          {
            date: "2016-11-27",
            activity_id: 3,
            result: "Not Good"
          }
        ]
      }
    ];

    return {
      request: function(config) {
        /*
          catch and rewrite the url request
         */
        if(config.url.indexOf('mock-api-data')!==-1){
          config.url = '#'+config.url;
        }
        return config;
      },

      requestError: function(config) {
        return config;
      },

      response: function(response) {
        /*
          Check if the url was rewrited and prepar the reponse in the way need for that entity.
         */
        if(response.config.url.indexOf('mock-api-data')!==-1){
          if(response.config.url.indexOf('definitions/activities')!==-1){
            //response.data = { name: 'Definition activities' };
            response.data = [{
                id: 1,
                intensity: "moderate",
                detail:[{
                  type: "moderate",
                  minutes: 150
                }]
              },{
                id: 2,
                intensity: "vigorous",
                detail:[{
                  type: "vigorous",
                  minutes: 75
                }]
              },{
                id: 3,
                intensity: "proportionate",
                detail:[{
                  type: "vigorous",
                  minutes: 25
                },
                {
                  type: "moderate",
                  minutes: 100
                }]
              }
            ];
          } else if (response.config.url.indexOf('patients')!==-1 && response.config.url.indexOf('summary')==-1){
            response.data = patients_list;
          } else if (response.config.url.indexOf('summary')!==-1 && response.config.url.indexOf('summary')!==-1){
            response.data = _.filter(patients_list, {id:parseInt(response.config.url.split('/')[3])})[0].summary;
          }
        }

        return response;
      },

      responseError: function(response) {
        return response;
      }
    }
  }

})();


