(function() {
  'use strict';
  
  /*
  	We need to register the intersepot into the $httpProvider for the app module mclaren
   */

  /*eslint angular/di: [2,"array"]*/
  angular
    .module('mclaren')
    .config(['$httpProvider', function ($httpProvider) {
      $httpProvider.interceptors.push('httpInterceptor');
    }]);

})();
