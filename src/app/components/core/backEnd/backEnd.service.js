(function() {
  'use strict';

  angular
    .module('mclaren')
    .factory('backEnd', backEnd);

  /*
    backEnd Service is in charge to communicate with the BackEnd resourec, 
    for the purpose of this test I creted only a generic get function in order to retrieve a list of object for every called entities.
   */

  /** @ngInject */
  function backEnd($log, $q, $http) {
    var apiHost = '/mock-api-data';

    var service = {      
      get: get
    };

    return service;

    function get(element) {      
      var d = $q.defer();
      
      $http.get(apiHost + '/' + element).then(function(response){
        d.resolve(response.data); 
      }, function(error){
        $log.error("Error in MainController: "* error);
      });

      return d.promise;
    }
  }
})();
