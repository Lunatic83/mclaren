(function() {
  'use strict';

  describe('service backEnd', function() {
    var backEnd;
    var $httpBackend;
    var $log;
    var element;

    beforeEach(module('mclaren'));
    beforeEach(inject(function(_backEnd_, _$httpBackend_, _$log_) {
      backEnd = _backEnd_;
      $httpBackend = _$httpBackend_;
      $log = _$log_;
      element = 'patients';
    }));

    it('should be registered', function() {
      expect(backEnd).not.toEqual(null);
    });

    describe('apiHost variable', function() {
      it('should exist', function() {
        expect(backEnd.apiHost).not.toEqual(null);
      });
    });

    describe('get function', function() {
      it('should exist', function() {
        expect(backEnd.get).not.toEqual(null);
      });

      it('should return data', function() {
        $httpBackend.when('GET',  '#/mock-api-data' + '/'+element).respond(200, [{pprt: 'value'}]);
        var data;
        backEnd.get('patients').then(function(fetchedData) {
          data = fetchedData;
        });
        $httpBackend.flush();
        expect(data).toEqual(jasmine.any(Array));
        expect(data.length).toBeTruthy();
        expect(data[0]).toEqual(jasmine.any(Object));
      });

      it('should log a error', function() {
        $httpBackend.when('GET',  '#/mock-api-data' + '/1').respond(500);
        backEnd.get(1).then(function() {}, function(){
          expect($log.error.logs).toEqual(jasmine.stringMatching('XHR Failed for'));
        });
        $httpBackend.flush();        
      });
    });
  });
})();
