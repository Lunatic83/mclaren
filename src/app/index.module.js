(function() {
  'use strict';

  angular
    .module('mclaren', ['ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize', 'ngMessages', 'ngAria', 'ui.router', 'ngMaterial', 'md.data.table']);

})();
