(function() {
  'use strict';

  describe('controllers', function(){
    var vm;
    var definitionActivities, patients, summary;
    var $q, $rootScope;
    var mockBackEnd, mockSummaryDialog, summaryDialogCalled;

    beforeEach(module('mclaren'));
    beforeEach(inject(function(_$rootScope_, _$controller_, _$q_) {
      $rootScope = _$rootScope_;
      $q = _$q_;

      definitionActivities = [{
                id: 1,
                intensity: "moderate",
                detail:[{
                  type: "moderate",
                  minutes: 150
                }]
              },{
                id: 2,
                intensity: "vigorous",
                detail:[{
                  type: "vigorous",
                  minutes: 75
                }]
              },{
                id: 3,
                intensity: "proportionate",
                detail:[{
                  type: "vigorous",
                  minutes: 25
                },
                {
                  type: "moderate",
                  minutes: 100
                }]
              }
            ];

      patients = [{
        id:1,
        patient_id: "0000-4444",
        first_name: "John",
        last_name: "Doe",
        gender: "Male",
        date_of_birth: "1960-02-22",
        race: "White",
        language: "English",
        maritial_status: "Divorced",
        summary:[
          {
            date: "2016-12-08",
            activity_id: 2,
            result: "Good"
          },
          {
            date: "2016-12-07",
            activity_id: 2,
            result: "Good"
          }
        ]
      },
      {
        id:2,
        patient_id: "0000-4445",
        first_name: "Maria",
        last_name: "Ross",
        gender: "Female",
        date_of_birth: "1965-06-12",
        race: "Black",
        language: "English",
        maritial_status: "Single",
        summary:[
          {
            date: "2016-12-08",
            activity_id: 2,
            result: "Good"
          },
          {
            date: "2016-12-07",
            activity_id: 1,
            result: "Not Good"
          }
        ]
      }];

      summary = [{
            date: "2016-12-08",
            activity_id: 2,
            result: "Good"
          },
          {
            date: "2016-12-07",
            activity_id: 1,
            result: "Not Good"
          }
      ];

      mockBackEnd = {
        get: function(url){
          var d = $q.defer();
          if(url == 'patients/1/summary'){
            d.resolve(summary);
          }else{
            d.reject(false);
          }
          return d.promise;    
        }
      }

      summaryDialogCalled = false;
      mockSummaryDialog = {
        show: function(){
          summaryDialogCalled = true;
          return null;
        }
      }

      vm = _$controller_('MainController', { 'backEnd': mockBackEnd, 'summaryDialog': mockSummaryDialog, 'definitionActivities': definitionActivities, 'patients': patients, 'summary':summary});
    }));

    it('should have a title variable', function() {
      expect(vm.title).toEqual(jasmine.any(String));
    });
    
    describe('getLast3DaysConditions tests', function() {
      it('should exist', function() {
        expect(vm.getLast3DaysConditions).not.toEqual(null);
      });      

      it('should return true', function() {
        var result = vm.getLast3DaysConditions(summary);
        expect(result).not.toBeTruthy();
      });

      it('should return false', function() {
        var summaryClone = _.cloneDeep(summary);
        summaryClone[1].result = "Good";
        var result = vm.getLast3DaysConditions(summaryClone);
        expect(result).toBeTruthy();
      });
    });

    describe('clickRow tests', function() {
      it('should exist', function() {
        expect(vm.clickRow).not.toEqual(null);
      });      

      it('should open summaryDialog', function() {
        vm.clickRow(1, 'Gaspare', 'Scherma');
        $rootScope.$apply();

        expect(summaryDialogCalled).toBeTruthy();
      });      

      it('should not open summaryDialog', function() {
        vm.clickRow(100, 'Gaspare', 'Scherma');
        $rootScope.$apply();

        expect(summaryDialogCalled).not.toBeTruthy();
      });      

    });


  });
})();
