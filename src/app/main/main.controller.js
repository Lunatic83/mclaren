(function() {
  'use strict';

  angular
    .module('mclaren')
    .controller('MainController', MainController);

  /*
    This is the only controller for the purpose of this test, the logic used for the developement is to keep it simple. 
    This controllore has only two functions binded with the view.
    clickRow and getLast3DaysConditions
   */


  /** @ngInject */
  function MainController($log, backEnd, summaryDialog, definitionActivities, patients, summary) {
    var vm = this;

    vm.title = 'McLaren test - Gaspare Scherma';
    vm.definitionActivities = definitionActivities;
    vm.patients = patients;
    vm.summary = summary;

    vm.clickRow = clickRow;
    vm.getLast3DaysConditions = getLast3DaysConditions;

    vm.query = {
      order: 'first_name',
      limit: 5,
      page: 1
    };

    activate();

    function activate() {      
    }

    /*
      getLast3DaysConditions calculate if insied the last 3 object of the summary array
      in this case an object is a day, and return false, if any of this last days as a Not Godd result
      in order to put in evidence into the view using ng-class that show a light red highlighted row
     */
    function getLast3DaysConditions(summary){
      if(_.includes(_.map(_.slice(summary,0,3), 'result'),"Not Good")){
        return false;
      } else {
        return true;
      }
    }

    /*
      callback event function for showing a patients summary activities
      This function receive the patient id the first_name and last_name as parameters
      This function is performing the operazion in the following order:
      - call the backEnd get for the patient waiting the async response for this call
        -  if the promise is resolved the summary will contain the list of the activity for that patient
          - for each activity in the summary, will be completed these information with the definition activities and added to a morre complete summary list.
          - at the end the summaryDialog will be request to be shown providing the first and last name for the summaryDialo header 
        - it the promise is reject nothing at UI interface is going to happen, only the warning log in the console, will happer, 
          IMPORTANT (this can be completed using a simple dialog showing a message to the user.)
     */
    function clickRow(id, first_name, last_name){
      backEnd.get('patients/'+id+'/summary').then(function(summary){        
        var summary_completed = [];
        _.each(summary, function(daily_activity){
            var activity = _.filter(definitionActivities, {id:daily_activity.activity_id});          
            var daily_activity_new = _.cloneDeep(daily_activity);
            delete daily_activity_new.activity_id;
            daily_activity_new.activity_detail = activity;
            summary_completed.push(daily_activity_new);
        });
        
        summaryDialog.show(first_name, last_name, summary_completed);
      }, function(error){
        // Future development show a simple dialog showing a message to the user.
        $log.warn("Error in MainController: "* error);
      });
    }
  }
})();

