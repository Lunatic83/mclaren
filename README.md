# README #

McLaren Gaspare Scherma test.

### How do I get set up? ###

In order to run this test 

run:

```
#!bash

$ git clone https://Lunatic83@bitbucket.org/Lunatic83/mclaren.git
$ cd mclaren
$ npm install
$ bower install

$ gulp serve      (for running the server and see the test)
$ gulp test       (for running the unit test suite)

```

This test was developed using:

* [Material Angularjs](https://material.angularjs.org/)

* [Fountainjs](http://fountainjs.io/)

* angular-material-data-table [md-data-table](https://github.com/daniel-nagy/md-data-table)


I created only a simple interface using a single MainController which include directives and services, this shows a table with all the patient information.

This table can be searched locally using the search input "search", the table has pagination, and can be sorted by column clicking on the column header.

Is possible to see the activity summary for a specific patient clicking to a row, a dialog will appear showing all the daily activity.
If you want to click into a specific daily activity will be showed the detail for that activity.

If something is not working please send an email to gaspare.scherma@gmail.com

Many thanks.